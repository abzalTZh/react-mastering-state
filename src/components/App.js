import BigCommunity from './bigCommunity';
import JoinProgram from './joinProgram';
import '../styles/App.css';

function App() {
  return (
    <>
      <BigCommunity />
      <JoinProgram />
    </>
  );
}

export default App;
