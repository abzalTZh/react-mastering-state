import { useState, useEffect } from 'react';

function UserSection( { avi, desc, fName, lName, pos } ) {
    return (
        <>
            <div className="app-section__user">
                <img className="app-section__user-avi" src={avi.replace('localhost:3000', 'localhost:4000')} alt={`${fName} ${lName} avatar`} />
                <p className="app-section__user-desc">
                    {desc}
                </p>
                <p className="app-section__user-name">{`${fName} ${lName}`}</p>
                <p className="app-section__user-pos">{pos}</p>
            </div>
        </>

    );
}

function BigCommunity() {
    const [data, setData] = useState(null);
    const [visible, setVisible] = useState(true);
    const commSectionContent = document.querySelector('div.section-content');

    const handleSectionToggleButton = (e) => {
        setVisible(!visible);
        sectionToggle();
    }

    const sectionToggle = () => {
        visible ? commSectionContent.style.display = 'none' : commSectionContent.style.display = 'block';
    }

    useEffect(() => {
        fetch(`http://localhost:4000/community`)
            .then((response) => response.json())
            .then(setData);
    }, []);

    if (data) {
        return (
            <section className="app-section app-section--community">
                <div className='section-title'>
                    <h1 className="app-title">Big Community of <br /> People Like You</h1>
                    <button className="app-section__hide--toggle" type="button" onClick={handleSectionToggleButton}>Hide section</button>
                </div>
                <div className="section-content">
                    <h2 className="app-subtitle">
                        We’re proud of our products, and we’re really excited when <br />
                        we get feedback from our users.
                    </h2>
                    <div className="app-section__users-container">
                        <UserSection
                            avi={data[0].avatar}
                            desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor."
                            fName={data[0].firstName}
                            lName={data[0].lastName}
                            pos={data[0].position}
                        />
                        <UserSection
                            avi={data[1].avatar}
                            desc="Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut."
                            fName={data[1].firstName}
                            lName={data[1].lastName}
                            pos={data[1].position}
                        />
                        <UserSection
                            avi={data[2].avatar}
                            desc="Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
                            fName={data[2].firstName}
                            lName={data[2].lastName}
                            pos={data[2].position}
                        />
                    </div>
                </div>
            </section>
        );
    }
}

export default BigCommunity;