import { useState, useEffect } from 'react';


function JoinProgram() {
    const emailInput = document.querySelector('input.app-form__email-input');
    const subButton = document.querySelector('button.app-section__button--subscribe');
    const [email, setEmail] = useState('');
    const [subscribed, setSubscribed] = useState(false);

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    }

    const subscribe = async () => {
        if (email !== '') {
            const response = await fetch(`http://localhost:4000/subscribe`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({email}),
            })
            .then(console.log('Subscribed successfully'))
            .catch(err => console.error(err));

            if (!response.ok) {
                window.alert(response.statusText);
                return;
            }

            setSubscribed(true);

            emailInput.style.display = 'none';
            subButton.textContent = 'Unsubscribe';
        }
    };

    const unsubscribe = async () => {
        fetch(`http://localhost:4000/unsubscribe`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({email}),
        })
        .then(console.log('Unsubscribed successfully'))
        .catch(err => console.error(err));

        setSubscribed(false);

        emailInput.style.display = 'block';
        subButton.textContent = 'Subscribe';
    }

    const handleSubscribeSubmit = (e) => {
        e.preventDefault();

        if(!subscribed) {
            subscribe();
            disableBtn();
            setTimeout(enableBtn, 1000);
        } else {
            unsubscribe();
            disableBtn();
            setTimeout(enableBtn, 1000);
        }
    }

    const disableBtn = () => {
        subButton.setAttribute('disabled', 'disabled')
        subButton.style.opacity = '0.5'
      }
      const enableBtn = () => {
        subButton.removeAttribute('disabled')
        subButton.style.opacity = '1'
      }

    useEffect(() => {
        
    }, [email])

    return (
        <section className="app-section app-section--image-join">
            <h1 className="app-title">Join Our Program</h1>
            <h2 className="app-subtitle">
                Sed do eiusmod tempor incididunt <br />
                ut labore et dolore magna aliqua. 
            </h2>
            <form className="app-form" onSubmit={handleSubscribeSubmit}>
                <input className="app-form__email-input" type="email" name="email" placeholder="Email" value={email} onChange={handleEmailChange} />
                <button
                    className="app-section__button app-section__button--subscribe" type="submit"
                >Subscribe</button>
            </form>
        </section>
    );
}

export default JoinProgram;